import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shimmer/shimmer.dart';
import 'package:rich_alert/rich_alert.dart';
class TaskReport extends StatefulWidget {
  TaskReport(this.listType);
  final String listType;
  @override
  HomePageState createState() => new HomePageState();
}
class Station {
  int total;
  Station(this.total);
  Station.fromJson(Map<String, dynamic> json) {
    total= json['total'];
  }
}
class HomePageState extends State<TaskReport> {
  List data;
  List datas;
  var title;
  var total;
  var delivery = 30;
  bool reverseSort = false;
  List<Station> values = List<Station>();
  List<Station> SearchDisplay = List<Station>();
  TextEditingController Fromdatecontroller = new TextEditingController();
  TextEditingController Todatecontroller = new TextEditingController();
  ScrollController  scrollController;
  bool ontop = true;
//  Future<List<Station>> fetchNotes() async {
//    var url = "http://smartshop.joyinfotech.online/api/v3/carts/";
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//     String token = prefs.getString("JWT");
//    var response = await http.get(url,headers: {HttpHeaders.authorizationHeader: "Token " +token},);
//    var notes = List<Station>();
//    if (response.statusCode == 200) {
//      data = json.decode(response.body)['orderResponses'];
//      for (var noteJson in data) {
//        notes.add(Station.fromJson(noteJson));
//      }
//
//
//    }
//    return notes;
//  }
  Future<String> getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("JWT");
    String Carts = prefs.getString("Cart");
    var url = "http://smartshop.joyinfotech.online/api/v3/cartss/";
    var response = await http.get(url+Carts,headers: {HttpHeaders.authorizationHeader: "Token " +token},);
    this.setState(() {
      data = json.decode(response.body)['orderResponses'];



    });
    print(data[1]["name"]);
    return "Success!";
  }
   getDatas() async {
    var url = "http://smartshop.joyinfotech.online/api/v3/cartss/";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String Carts = prefs.getString("Cart");
    String token = prefs.getString("JWT");
    var response = await http.get(url+Carts,headers: {HttpHeaders.authorizationHeader: "Token " +token},);
    this.setState(() {
      total = json.decode(response.body)['cart']['total'].toString();
      title = json.decode(response.body)['orderResponses']['product']['title'];
    });
    print(total);
    print(data[1]["name"]);
    return "Success!";
  }
  @override
  void initState() {
    this.getData();
    getDatas();
//    fetchNotes().then((value) {
//      setState(() {
//        values.addAll(value);
//        SearchDisplay = values;
//      });
//    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    if (data  == null) {
      return Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Column(
            children: [0, 1, 2, 3, 4, 5, 6, 7]
                .map((_) =>
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 48.0, height: 48.0, color: Colors.white,),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),),
                      Expanded(child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(width: double.infinity,
                            height: 8.0,
                            color: Colors.white,),
                          Padding(padding: const EdgeInsets.symmetric(
                              vertical: 2.0),),
                          Container(width: double.infinity,
                            height: 8.0,
                            color: Colors.white,),
                          Padding(padding: const EdgeInsets.symmetric(
                              vertical: 2.0),),
                          Container(
                            width: 40.0, height: 8.0, color: Colors.white,),
                        ],
                      ),
                      )
                    ],
                  ),
                ))
                .toList(),
          ),
        ),
      );
    }

    else {
      return Scaffold(
          body:  SafeArea(
          child: Column(
           mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
           Container(
          padding: EdgeInsets.symmetric(horizontal:16.0, vertical: 30.0),
          child: Text("Purchase  Details", style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
              color: Colors.grey.shade700
          ),)),
             Expanded( child: ListView.builder(
                    itemBuilder: (context, index) {
                    return Stack(
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            margin: EdgeInsets.only(right: 30.0, bottom: 10.0),
                            child: Material(
                              borderRadius: BorderRadius.circular(5.0),
                              elevation: 3.0,
                              child: Container(
                                padding: EdgeInsets.all(16.0),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                           Container(
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text("Quantity: " + "${data[index]['quantity'].toString()}", style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 18.0
                                                ),),
                                              ],),),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            top: 20,
                            right: 15,
                            child: Container(
                              height: 30,
                              width: 30,
                              alignment: Alignment.center,
                              child: MaterialButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                                padding: EdgeInsets.all(0.0),
                                color: Colors.pinkAccent,
                                child: Icon(Icons.clear, color: Colors.white,),
                                onPressed: () {},
                              ),
                            ),
                          )
                        ],
                      );
                    },
                    itemCount: data.length ,
                  ),),
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(20.0),
          child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text("Subtotal      \₹: $total".toString()
                      , style: TextStyle(
                      color: Colors.grey.shade700,
                      fontSize: 16.0
                  ),),
                  SizedBox(height: 5.0,),
                  Text("Delivery       \ free", style: TextStyle(
                      color: Colors.grey.shade700,
                      fontSize: 16.0
                  ),),
                  SizedBox(height: 10.0,),
                  Text("Cart Subtotal     \₹: $total", style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0
                  ),),
                  SizedBox(height: 20.0,),
                  SizedBox(
                    width: double.infinity,
                    child: MaterialButton(
                      height: 50.0,
                      color: Colors.pinkAccent,
                      child: Text("BUY NOW".toUpperCase(), style: TextStyle(
                          color: Colors.white
                      ),),
                      onPressed: () {
                        descriptionDetails();
                      },
                    ),
                  )
                ],
              ),


    )],
    ),),
      );

       }

      }
  void descriptionDetails() {
    var alert = new AlertDialog(
      title: new Text('Confirm Order'),
      content:Form(
        child: SingleChildScrollView(
          child:Column(mainAxisSize:MainAxisSize.min,
            children:<Widget>[
              new ListTile(leading: const Icon(Icons.location_on),
                title: new TextFormField(validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter Address';
                  }
                },
                  decoration: new InputDecoration(hintText: "Address",),
                ),
              ),
              new ListTile(leading: const Icon(Icons.location_city),
                title: new TextFormField(validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter city';
                  }
                },
                  decoration: new InputDecoration(hintText: "City",),
                ),
              ),
              new ListTile(leading: const Icon(Icons.phone),
                title: new TextFormField(validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter Phone No';
                  }
                },
                  decoration: new InputDecoration(hintText: "Phone",),
                ),
              ),
            ]
            ,),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: const Text('Confirm Order'),
          onPressed: () async  {
            loginSucessAlertBox();
          },
        ),
      ],
    );
    // ignore: deprecated_member_use
    showDialog(context: context, child: alert);
  }
  void loginSucessAlertBox() {
    showDialog(
        context: context,
        builder: (context) {
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).pop(true);
          });
          return RichAlertDialog( //uses the custom alert dialog
            alertTitle: Text("Thank you for Your Order "),
            alertSubtitle: richSubtitle("Delivery within a day"),
            alertType: RichAlertType.SUCCESS,
            actions: <Widget>[
              Text('Good Way')
            ],
          );
        });
  }
}
