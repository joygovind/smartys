import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:smarty/assets.dart';
import 'package:smarty/network_image.dart';
import 'package:smarty/login.dart';
void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title:' Smart Shopping',
      debugShowCheckedModeBanner:false,
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Mainpage(title: 'Smart Shopping'),
    );
  }
}
class Mainpage extends StatelessWidget {
  Mainpage({Key key, this.title}) : super(key: key);
  final String title;
  final List<String> sliderItems = [
    rice,
    oil,
    dalls,
    Soaps,snacks,tea
  ];
  final List<Map> orderss = [
  {
  "image" : orders,
},];
  final List<Map> restaurants = [
    {
      "image" : rice,
      "name":"Rice",
      "specials":"Jeera,Basmathi rice etc"
    },
    {
      "image" : oil,
      "name":"OIL",
      "specials":"Refined oils etc"
    },
    {
      "image" : snacks,
      "name":"Snacks",
      "specials":"Chips,Biscuits etc"
    },
    {
      "image" : Soaps,
      "name":"Soaps",
      "specials":"Tooth paste etc"
    },
    {
      "image" : tea,
      "name":"Coffee & Tea",
      "specials":"Cofees ,Green Tea"
    },
    {
      "image" : dalls,
      "name":"Dal",
      "specials":"All dals available"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,

      body: CustomScrollView(
        slivers: <Widget>[
          _buildAppBar(context),
          _buildSlider(),
          _buildListSectionHeader(context,"Order Your Need Items"),
          _buildPopularRestaurant(),
          _buildListSectionHeader1(context),

        ],
      ),
    );
  }

  SliverToBoxAdapter _buildSlider() {
    return SliverToBoxAdapter(
      child: Stack(
        children: <Widget>[
          Container(
            height: 200,
            child: Swiper(
              itemCount: sliderItems.length,
              autoplay: true,
              curve: Curves.easeIn,
              itemBuilder: (BuildContext context, int index){
                return PNetworkImage(sliderItems[index], fit: BoxFit.cover);
              },
            ),
          ),
          Container(
            height: 200,
            color: Colors.black.withOpacity(0.5),
          ),
          Positioned(
            bottom: 20,
            left: 20,
            child: Text("Heavy discount Availble Order today only.", style: TextStyle(
                color: Colors.white
            )),
          )
        ],
      ),
    );
  }

  SliverAppBar _buildAppBar(BuildContext context) {
    return SliverAppBar(
      textTheme: TextTheme(
          // ignore: deprecated_member_use
          title: Theme.of(context).textTheme.title.merge(TextStyle(color: Colors.black))
      ),
      iconTheme: IconThemeData(color: Colors.lightGreen),
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      expandedHeight: 130,
      floating: true,
      flexibleSpace: Container(
        height: 160,
        padding: EdgeInsets.only(left: 20.0,right: 20.0, top: 30.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(child: Text("Deliver to")),
                  IconButton(icon: Icon(Icons.shopping_cart), onPressed: (){
                    Navigator.push(context, MaterialPageRoute(
                      builder: (BuildContext context) => PlaceList1()
                  ));},)
                ],
              ),
              SizedBox(height: 5.0,),
              TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                    hintText: "Search for items  List",
                    suffixIcon: Icon(Icons.search)
                ),

              )
            ],
          ),
        ),
      ),
    );
  }

  SliverToBoxAdapter _buildListSectionHeader(BuildContext context, String title) {
    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.only(left: 20.0,top: 20.0),
        // ignore: deprecated_member_use
        child: Text(title,style: Theme.of(context).textTheme.subtitle,),
      ),
    );
  }
  SliverToBoxAdapter _buildListSectionHeader1(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.only(left: 20.0,top: 20.0),
        // ignore: deprecated_member_use
         child: new RaisedButton(
          elevation: 0.0,
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(30.0)),
          padding: EdgeInsets.only(
              top: 7.0, bottom: 7.0, right: 40.0, left: 7.0),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) => PlaceList1()));
          },
          child: new Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Image.asset('assets/order',
                  height: 40.0, width: 40.0),
              Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: new Text(
                    "Order now ",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0),
                  ))
            ],
          ),
          textColor: Color(0xFF292929),
          color: Colors.blueAccent),
    ),
    );
  }
  SliverGrid _buildPopularRestaurant() {
    return SliverGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2
      ),
      delegate: SliverChildBuilderDelegate((BuildContext context, int index){
        return Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  height: 130.0,
                  width: double.infinity,
                  child: PNetworkImage(restaurants[index]["image"], fit: BoxFit.cover )),
              // ignore: deprecated_member_use
              Container(
                height: 30.0,
                width: double.infinity,
                  child: MaterialButton(
                    color: Colors.pinkAccent,
                    // ignore: deprecated_member_use
                    child:  Text(restaurants[index]["name"], style: Theme.of(context).textTheme.title.merge(TextStyle(
                        fontSize: 14.0, color: Colors.white,
                    ))),
                    onPressed: () {Navigator.push(context, MaterialPageRoute(
                        builder: (BuildContext context) => PlaceList1()
                    ));},
                  ),),
            ],
          ),
        );
      },
          childCount: restaurants.length
      ),
    );
  }
  void _openDetailPage(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(
        builder: (BuildContext context) => PlaceList1()
    ));
  }
}