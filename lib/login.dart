import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:page_transition/page_transition.dart';
import 'package:smarty/products.dart';
import 'package:smarty/carts.dart';
import 'package:smarty/Orders.dart';
import 'package:smarty/main.dart';
class Response {
  final String token;
  Response({this.token});
  factory Response.fromJson(Map<String, dynamic> json) {
    return Response(
      token: json["token"],
    );
  }
}
Future<Response> post(String url, var body, var headers)async{
  return await http
      .post(Uri.encodeFull(url), body: body, headers: headers)
      .then((http.Response response) {
    final int statusCode = response.statusCode;
    if (statusCode == 200 || statusCode == 404) {
      return Response.fromJson(json.decode(response.body));
    }
    throw new Exception("Failed to post data.");
  });
}
class PlaceList1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: ' Smart Shopping',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue),
      home: LoginPage(title: 'Smart Shopping'),
    );
  }
}
class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  HomePageState createState() => HomePageState();
}
class HomePageState extends State<LoginPage>{
  bool formVisible;
  int _formsIndex;
  @override
  void initState() {
    super.initState();
    formVisible = false;
    _formsIndex = 1;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/store.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: <Widget>[
              Container(
                color: Colors.black54,
                child: Column(
                  children: <Widget>[
                    const SizedBox(height: kToolbarHeight + 40),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "Welcome",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 30.0,
                            ),
                          ),
                          const SizedBox(height: 10.0),
                          Text(
                            "Welcome to Smart Online Shopping. \n You are awesome",
                            style: TextStyle(
                              color: Colors.white70,
                              fontSize: 18.0,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    Row(
                      children: <Widget>[
                        const SizedBox(width: 10.0),
                        Expanded(
                          child: RaisedButton(
                            color: Colors.red,
                            textColor: Colors.white,
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Text("Login"),
                            onPressed: () {
                              setState(() {
                                formVisible = true;
                                _formsIndex = 1;
                              });
                            },
                          ),
                        ),
                        const SizedBox(width: 10.0),
                        Expanded(
                          child: RaisedButton(
                            color: Colors.grey.shade700,
                            textColor: Colors.white,
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Text("Signup"),
                            onPressed: () {
                              setState(() {
                                formVisible = true;
                                _formsIndex = 2;
                              });
                            },
                          ),
                        ),
                        const SizedBox(width: 10.0),
                      ],
                    ),
                    const SizedBox(height: 40.0),
                  ],
                ),
              ),
              AnimatedSwitcher(
                duration: Duration(milliseconds: 200),
                child: (!formVisible)
                    ? null
                    : Container(
                  color: Colors.black54,
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          RaisedButton(
                            textColor: _formsIndex == 1
                                ? Colors.white
                                : Colors.black,
                            color:
                            _formsIndex == 1 ? Colors.red : Colors.white,
                            child: Text("Login"),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            onPressed: () {
                              setState(() {
                                _formsIndex = 1;
                              });
                            },
                          ),
                          const SizedBox(width: 10.0),
                          RaisedButton(
                            textColor: _formsIndex == 2
                                ? Colors.white
                                : Colors.black,
                            color:
                            _formsIndex == 2 ? Colors.red : Colors.white,
                            child: Text("Signup"),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            onPressed: () {
                              setState(() {
                                _formsIndex = 2;
                              });
                            },
                          ),
                          const SizedBox(width: 10.0),
                          IconButton(
                            color: Colors.white,
                            icon: Icon(Icons.clear),
                            onPressed: () {
                              setState(() {
                                formVisible = false;
                              });
                            },
                          )
                        ],
                      ),
                      Container(
                        child: AnimatedSwitcher(
                          duration: Duration(milliseconds: 300),
                          child:
                          _formsIndex == 1 ? LoginForm() : SignupForm(),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
class LoginForm extends StatelessWidget {
  LoginForm({
    Key key,
  }) : super(key: key);
  final formkey = GlobalKey<FormState>();
  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(

      margin: const EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: Form(
        key: formkey,child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(16.0),
        children: <Widget>[
          TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                return ("please enter your emailid");
              }
            },
            controller: usernameController ,
            decoration: InputDecoration(
              hintText: "Enter your mobile no",
              border: OutlineInputBorder(),
            ),
          ),
          const SizedBox(height: 10.0),
          TextFormField(
            obscureText: true,
            validator: (value) {
              if (value.isEmpty) {
                return ("please enter your password");
              }
            },
            controller: passwordController ,
            decoration: InputDecoration(
              hintText: "Enter password",
              border: OutlineInputBorder(),
            ),
          ),
          const SizedBox(height: 10.0),
          RaisedButton(
            color: Colors.red,
            textColor: Colors.white,
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Text("Login"),
            onPressed: () {
              post('http://smartshop.joyinfotech.online/api/v3/auth/login', {'username': usernameController.text, 'password': passwordController.text}, {"Accept":"application/json"})
                  .then((result) async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                String token = result.token;
                prefs.setString("JWT", token);
                  Navigator.push(context, PageTransition(
                      child:  ExtractArgumentsScreen (), type: PageTransitionType.upToDown));
              } );
            },
          ),
        ],
      ),),
    );
  }

}

class SignupForm extends StatelessWidget {
  SignupForm({
    Key key,
  }) : super(key: key);
  final formkey = GlobalKey<FormState>();
  TextEditingController MobilenoController = new TextEditingController();
  TextEditingController mailidController = new TextEditingController();
  TextEditingController PasswordController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: formkey,
      child:Container(
        margin: const EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.all(16.0),
          children: <Widget>[
            TextField(
              controller: MobilenoController,
              decoration: InputDecoration(
                hintText: "Enter your Mobile no",
                border: OutlineInputBorder(),
              ),
            ),
            const SizedBox(height: 10.0),
            TextField(
              controller: mailidController,
              decoration: InputDecoration(
                hintText: "Enter your mail id",
                border: OutlineInputBorder(),
              ),
            ),
            const SizedBox(height: 10.0),
            TextField(
              controller: PasswordController,
              obscureText: true,
              decoration: InputDecoration(
                hintText: "Confirm password",
                border: OutlineInputBorder(),
              ),
            ),
            const SizedBox(height: 10.0),
            RaisedButton(
              color: Colors.red,
              textColor: Colors.white,
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Text("Signup"),
              onPressed: () {
                post('http://smartshop.joyinfotech.online/api/v3/auth/register', {'username': MobilenoController.text,
                  'email': mailidController.text, 'password':PasswordController.text}, {"Accept":"application/json"})
                    .then((result) async {
                } );
              },
            ),
          ],
        ),),
    );
  }

}
class ExtractArgumentsScreen extends StatelessWidget {
  static const routeName = '/extractArguments';
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Govig',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primaryColor: const Color(0xFF2196F3),
        primaryColorDark: const Color(0xFF90CAF9),
        accentColor: const Color(0xFF2196F3),
      ),
      home: new DashboardScreen(title: 'Govig'),
    );
  }
}
class DashboardScreen extends StatefulWidget {
  DashboardScreen({Key key, this.title}) : super(key: key);
  final String title;
  @override
  DashboardScreenStates createState() => new DashboardScreenStates();
}
class DashboardScreenStates extends State<DashboardScreen> {
  final GlobalKey<ScaffoldState> keys = GlobalKey<ScaffoldState>();
  final Color primary = Colors.white;
  final Color active = Colors.grey.shade800;
  final Color divider = Colors.grey.shade600;
  PageController _pageController;
  int _page = 0;
  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }
  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }
  void navigationTapped(int page) {
    _pageController.animateToPage(page,
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }
  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: keys,
      appBar: new AppBar(
        title:Row(mainAxisAlignment: MainAxisAlignment.center,children: [
          Image.asset('assets/logo.png', fit: BoxFit.contain, height: 32,),
          Container(padding: const EdgeInsets.all(2.0), child: Text(widget.title),
          )
        ],),
        leading: IconButton(icon: Icon(Icons.menu),
          onPressed: () {
            keys.currentState.openDrawer();
          },
        ),
      ),
      drawer: buildDrawer(),
      body: new PageView(
        children: [
          new Invoice("Home screen"),
          new TaskReport("Cart screen"),
          new Reports("Reports")
        ],
        onPageChanged: onPageChanged,
        controller: _pageController,
      ),
      bottomNavigationBar: new Theme(
        data: Theme.of(context).copyWith(
          canvasColor: const Color(0xFF2196F3),
        ),
        child: new BottomNavigationBar(
          items: [
            new BottomNavigationBarItem(
                icon: new Icon(
                  Icons.home,
                  color: const Color(0xFFFFFFFF),
                ),
                title: new Text(
                  "Products",
                  style: new TextStyle(
                    color: const Color(0xFFFFFFFF),
                  ),
                )),
            new BottomNavigationBarItem(
                icon: new Icon(
                  Icons.shopping_cart,
                  color: const Color(0xFFFFFFFF),
                ),
                title: new Text(
                  "Carts",
                  style: new TextStyle(
                    color: const Color(0xFFFFFFFF),
                  ),
                )),
            new BottomNavigationBarItem(
              icon: new Icon(
                Icons.history,
                color: const Color(0xFFFFFFFF),
              ),
              title: new Text(
                "Orders",
                style: new TextStyle(
                  color: const Color(0xFFFFFFFF),
                ),
              ),
            )
          ],
          onTap:  navigationTapped,
          currentIndex: _page,
        ),
      ),
    );
  }
  buildDrawer() {
    return Drawer(
      child: Container(
        padding: const EdgeInsets.only(left: 16.0, right: 40),
        decoration: BoxDecoration(
            color: primary, boxShadow: [BoxShadow(color: Colors.black45)]),
        width: 300,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerRight,
                  child: IconButton(icon: Icon(Icons.power_settings_new, color: active,),
                    onPressed:  ()  async {
                      final prefs =await SharedPreferences.getInstance();
                      await prefs.clear();
                      Navigator.push(context, PageTransition(
                          child:  MyApp(), type: PageTransitionType.upToDown));

                    },
                  ),
                ),
                SizedBox(height: 5.0),
                new CircleAvatar(backgroundImage: AssetImage("assets/logo.png",),),
                Text("Govig", style: TextStyle(color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.w600),),
                SizedBox(height: 30.0),
                new ListTile(
                  title: new Text("Profile"),
                  onTap: () {

                  },
                ),
                new ListTile(
                  title:new Text("Terms and conditions"),
                  onTap:(){

                  },
                ),
                buildDivider(),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Divider buildDivider() {
    return Divider(
      color: divider,
    );
  }
}




