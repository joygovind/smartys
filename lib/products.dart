import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shimmer/shimmer.dart';
import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_counter/flutter_counter.dart';
import 'package:shared_preferences/shared_preferences.dart';
class Entry{
  final String order;
  final String detail;
  Entry({this.order,this.detail});
  factory Entry.fromJson(Map <String, dynamic> json){
    return Entry(
      order: json['order']['cart'].toString(),
      detail:json['details']
    );
  }
}
Future<Entry> posts(String url, var body, var headers)async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("JWT");
  return await http.post(Uri.encodeFull(url), body: body,  headers: {HttpHeaders.authorizationHeader: "Token " +token},).then((http.Response response) {
    final int statusCode = response.statusCode;
    if (statusCode >= 200 || statusCode == 404) {
      print('loggging in');
      return Entry.fromJson(json.decode(response.body));
    }
    throw new Exception("Failed to post data.");
  });
}
class Invoice extends StatefulWidget {
  Invoice(this.listType);
  final String listType;
  @override
  HomePageState createState() => new HomePageState();
}
class HomePageState extends State<Invoice> {
  TextEditingController quantitycontroler = new TextEditingController();
  List data;

  // Function to get the JSON data
  Future<String> getJSONData() async {
    var response = await http.get(
      // Encode the url
        Uri.encodeFull("http://smartshop.joyinfotech.online/api/v3/products/"),
        // Only accept JSON response
        headers: {"Accept": "application/json"}
    );

    setState(() {
      // Get the JSON data
      data = json.decode(response.body);
    });

    return "Successfull";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildListView(),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (context, index) {
          return _buildImageColumn(data[index]);
          // return _buildRow(data[index]);
        }
    );
  }

  Widget _buildImageColumn(dynamic item) => Container(
    decoration: BoxDecoration(
        color: Colors.white54
    ),
    margin: const EdgeInsets.all(4),
    child: Column(
      children: [
        new CachedNetworkImage(
          imageUrl: item['image'],
          placeholder: (context, url) => new CircularProgressIndicator(),
          errorWidget: (context, url, error) => new Icon(Icons.error),
          fadeOutDuration: new Duration(seconds: 1),
          fadeInDuration: new Duration(seconds: 3),
        ),
        _buildRow(item)
      ],
    ),
  );

  Widget _buildRow(dynamic item) {
    return ListTile(
      title: Text(
        item['title'] == null ? '': item['title'],
      ),
      // ignore: deprecated_member_use
      subtitle: Text("₹: " + item['price'].toString() ,style: Theme.of(context).textTheme.title.merge(TextStyle(fontSize: 16.0,color: Colors.red),)),
      trailing: new Icon(Icons.shopping_cart,color: Colors.red),
      onTap: () { descriptionDetails(context, item['id'].toString());},
    );
  }
  @override
  void initState() {
    super.initState();
    // Call the getJSONData() method when the app initializes
    this.getJSONData();
  }
  void descriptionDetails(BuildContext context, String message) {
    num _counter = 0;
    num _defaultValue = 0;
    var alert = new AlertDialog(
      title: new Text('Purchase Details'),
      content:Form(
        child: SingleChildScrollView(
          child:Column(mainAxisSize:MainAxisSize.min,
            children:<Widget>[
              new TextField(
                controller: quantitycontroler,
                decoration: new InputDecoration(labelText: "Enter your Quantity"),
                keyboardType: TextInputType.number
              ),
            ]
            ,),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: const Text('Add to Cart'),
          onPressed: () async  {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            String user = prefs.getString("user");
            String token = prefs.getString("JWT");
            String value= 'token ';
            print(token);
            print(message);
            print( 'Token '+ token);
            posts('http://smartshop.joyinfotech.online/api/v3/cart/add', {'product_id':message,
              'quantity': quantitycontroler.text},
                {"Accept":"application/json" })
                .then((result) async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              String order = result.order;
              prefs.setString("Cart", order);
              print('$value$token');
              print(result.detail);
              loginSucessAlertBox();
            } );
          },
        ),
      ],
    );
    // ignore: deprecated_member_use
    showDialog(context: context, child: alert);
  }
  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('JWT');
  }
  void loginSucessAlertBox() {
    showDialog(
        context: context,
        builder: (context) {
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).pop(true);
          });
          return AlertDialog(
            title: new Text('Thank You'),
          );
        });
  }


}

