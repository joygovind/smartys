import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
class Reports extends StatefulWidget {
  Reports(this.listType);
  final String listType;
  @override
  HomePageState createState() => new HomePageState();
}
class HomePageState extends State<Reports> {
  List data;
  Future<String> getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("JWT");
    final response = await http.get('http://smartshop.joyinfotech.online/api/v3/OrdersCart/');
    this.setState(() {
      data = json.decode(response.body);
    });
    print(data[1]["name"]);
    return "Success!";
  }

  @override
  // ignore: must_call_super
  void initState(){
    this.getData();

  }
  @override
  Widget build(BuildContext context){
    if(data == null ) {
      return Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Column(
            children: [0, 1, 2, 3, 4, 5, 6,7]
                .map((_) => Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(width: 48.0, height: 48.0, color: Colors.white,),
                  Padding(padding: const EdgeInsets.symmetric(horizontal: 8.0),),
                  Expanded(child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(width: double.infinity, height: 8.0, color: Colors.white,),
                      Padding(padding: const EdgeInsets.symmetric(vertical: 2.0),),
                      Container(width: double.infinity, height: 8.0, color: Colors.white,),
                      Padding(padding: const EdgeInsets.symmetric(vertical: 2.0),),
                      Container(width: 40.0, height: 8.0, color: Colors.white,),
                    ],
                  ),
                  )
                ],
              ),
            ))
                .toList(),
          ),
        ),
      );
    }
    else {
      return Container(
        child : ListView.builder(
          itemCount: data.length ,
          itemBuilder: (BuildContext context, int index) {
            return  new Card(
                child:ExpansionTile(
                    title: new Text("${data[index]['product'].toString()} ",
                        style: new TextStyle(fontSize: 18.9, color: Colors.black87)),
                    subtitle:new Text("₹: " +"${data[index]['price'].toString()} ",
                        style: new TextStyle(fontSize: 18.9, color: Colors.black87)),
                    leading: new CircleAvatar(backgroundColor: Colors.greenAccent,
                        child: new Icon(Icons.check_circle) ),
                    children: <Widget>[
                      new ListTile(
                        title:new Text("Payment: " +"${data[index]['paid'].toString()} ",
                            style: new TextStyle(fontSize: 18.9, color: Colors.black87)),
                        subtitle: new Text("Quantity: " +
                            "${data[index]['quantity']}",
                          style: new TextStyle(fontSize: 14.4, color: Colors.grey, fontStyle: FontStyle.italic),),
                      )
                    ]
                )
            );
          },
        ),
      );
    }
  }

}